#!/bin/sh

LOCAL_USER=$(whoami)

if [ "$LOCAL_USER" = "root" ]; then
    echo "Please run as the user you intend to log in as"
    exit 1
fi

echo "\n\n\nDAOS bootstrap starting\n\n"
sleep 1

BOOTSTRAP_ROOT='
fail() {
  echo "\n\n**** ERROR - BOOTSTRAP INCOMPLETE *****\n\nPress ENTER to continue\n"
  read TEMP
  exit 1
}

LOCAL_USER="$1"
export DISPLAY="$2"
export WAYLAND_DISPLAY="$3"
export DBUS_SESSION_BUS_ADDRESS="$4"
export XDG_CURRENT_DESKTOP="$5"
export XAUTHORITY="/home/$LOCAL_USER/.Xauthority"

apt-get update
apt-get upgrade -y
apt-get install -y ansible git || fail

rm -rf /usr/share/daos >/dev/null 2>&1 </dev/null || true
git clone https://gitlab.com/derek.hageman/daos-install.git /usr/share/daos || fail

cd /usr/share/daos || fail
ansible-playbook -i local_inventory -e "username=$LOCAL_USER" -e "xdg_gui=$XDG_CURRENT_DESKTOP" daos.yml || fail

sudo -E -u "$LOCAL_USER" /opt/forge/venv/bin/forge-acquisition-serial-setup --require-local-ports

echo "\n\n\nBootstrap finished.\n"
echo "Please reboot NOW before proceeding with any further setup"
read TEMP
'

if [ -t 0 ]; then
  exec sudo /bin/sh -c "$BOOTSTRAP_ROOT" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY" "$DBUS_SESSION_BUS_ADDRESS" "$XDG_CURRENT_DESKTOP"
elif command -v xfce4-terminal >/dev/null; then
  exec xfce4-terminal -x sudo /bin/sh -c "$BOOTSTRAP_ROOT" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY" "$DBUS_SESSION_BUS_ADDRESS" "$XDG_CURRENT_DESKTOP"
elif command -v lxterminal >/dev/null; then
  exec lxterminal -e sudo /bin/sh -c "$BOOTSTRAP_ROOT" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY" "$DBUS_SESSION_BUS_ADDRESS" "$XDG_CURRENT_DESKTOP"
elif command -v gnome-terminal >/dev/null; then
  exec gnome-terminal -- sudo /bin/sh -c "$BOOTSTRAP_ROOT" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY" "$DBUS_SESSION_BUS_ADDRESS" "$XDG_CURRENT_DESKTOP"
elif command -v konsole >/dev/null; then
  exec konsole -e sudo /bin/sh -c "$BOOTSTRAP_ROOT" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY" "$DBUS_SESSION_BUS_ADDRESS" "$XDG_CURRENT_DESKTOP"
elif command -v xterm >/dev/null; then
  exec xterm -e sudo /bin/sh -c "$BOOTSTRAP_ROOT" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY" "$DBUS_SESSION_BUS_ADDRESS" "$XDG_CURRENT_DESKTOP"
else
  exec sudo /bin/sh -c "$BOOTSTRAP_ROOT" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY" "$DBUS_SESSION_BUS_ADDRESS" "$XDG_CURRENT_DESKTOP"
fi
