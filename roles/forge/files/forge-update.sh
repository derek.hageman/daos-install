#!/bin/sh

set -e
umask 0022

cd /opt/forge
git fetch origin

LOCAL=$(git rev-parse HEAD)
REMOTE=$(git rev-parse origin/daos)

[ "${LOCAL}" = "${REMOTE}" ] && [ ! -e .incomplete_install ] && exit 0

git checkout -f origin/daos
touch .incomplete_install || true
/opt/forge/venv/bin/pip install --compile .
rm -f .incomplete_install || true
#find /opt/forge/forge/acquisition/systemd/units -mindepth 1 -maxdepth 1 -type f -exec cp \{\} /etc/systemd/system/ \;
#systemctl daemon-reload

systemctl try-restart forge-telemetry-uplink.service || true
systemctl try-restart forge-telemetry-tunnel.service || true

systemctl stop forge-vis-export.service || true
systemctl restart forge-vis-export.socket || true
systemctl stop forge-vis-realtime.service || true
systemctl restart forge-vis-realtime.socket || true
systemctl stop forge-vis-acquisition.service || true
systemctl restart forge-vis-acquisition.socket || true

systemctl try-restart forge-acquisition.service || true

systemctl stop forge-vis.service || true
systemctl restart forge-vis.socket || true
systemctl stop forge-dashboard.service || true
systemctl restart forge-dashboard.socket || true

