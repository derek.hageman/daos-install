#!/usr/bin/python3

import http.server
import ssl
import socket
import requests
from threading import Thread
from http import HTTPStatus
from functools import partial
from ipaddress import ip_address, IPv4Network


PROXY_ADDRESS_SPACE = IPv4Network('192.168.254.0/24')


class Stub(http.server.BaseHTTPRequestHandler):
    def _request(self):
        self.send_response_only(HTTPStatus.OK)
        self.send_header('Server', self.version_string())
        self.send_header('Date', self.date_time_string())
        self.end_headers()

    do_GET = _request
    do_HEAD = _request
    do_POST = _request
    do_PUT = _request


class Proxy(http.server.BaseHTTPRequestHandler):
    def __init__(self, *args, scheme='http', **kwargs):
        self.scheme = scheme
        super().__init__(*args, **kwargs)

    def _next_chunk(self):
        while True:
            n = self.rfile.readline()
            if not n:
                return None
            n = n.strip()
            if len(n) <= 0:
                continue
            n = int(n, 16)
            if n <= 0:
                break
            return self.rfile.read(n)

    def _request_data(self):
        try:
            if 'chunked' in self.headers['Transfer-Encoding']:
                result = bytes()
                while True:
                    chunk = self._next_chunk()
                    if not chunk:
                        break
                    result += chunk
                return result
        except (KeyError, ValueError):
            pass
        except (IOError, TypeError):
            return None
        try:
            n = int(self.headers['Content-Length'])
            return self.rfile.read(n)
        except (ValueError, IOError, KeyError, TypeError):
            return None

    def _request(self, method):
        self.send_response_only(HTTPStatus.OK)

        source_address = ip_address(self.client_address[0])
        if source_address.is_loopback:
            return
        if source_address in PROXY_ADDRESS_SPACE:
            return

        try:
            r = requests.request(method, self.scheme + "://" + self.headers['Host'] + "/" + self.path,
                                 timeout=3.0, data=self._request_data(), cookies={}, verify=False)
            for h, v in r.headers.items():
                if h == 'Set-Cookie':
                    continue
                self.send_header(h, v)
            self.end_headers()
            self.wfile.write(r.content)
        except (requests.RequestException, KeyError):
            self.send_header('Server', self.version_string())
            self.send_header('Date', self.date_time_string())
            self.end_headers()
        except ConnectionResetError:
            pass

    def do_GET(self):
        self._request("get")

    def do_HEAD(self):
        self._request("head")

    def do_POST(self):
        self._request("post")

    def do_PUT(self):
        self._request("put")


def _execute_server(address, HandlerClass):
    def _tls():
        with http.server.ThreadingHTTPServer((address, 443), partial(HandlerClass, scheme="https")) as httpd:
            httpd.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            httpd.socket = ssl.wrap_socket(httpd.socket,
                                           server_side=True,
                                           certfile='/var/lib/purpleair/proxy.crt',
                                           keyfile='/var/lib/purpleair/proxy.key',
                                           ssl_version=ssl.PROTOCOL_TLS)
            httpd.serve_forever()

    Thread(target=_tls).start()

    def _plain():
        with http.server.ThreadingHTTPServer((address, 80), partial(HandlerClass, scheme="http")) as httpd:
            httpd.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            httpd.serve_forever()

    Thread(target=_plain).start()


_execute_server('192.168.254.1', Stub)
_execute_server('192.168.254.2', Proxy)
