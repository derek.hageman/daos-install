#!/usr/bin/python3

import typing
import logging
import argparse
import zipfile
import os
import shutil
import pwd
import grp
import stat
from pathlib import Path
from json import loads as from_json

_LOGGER = logging.getLogger(__name__)


parser = argparse.ArgumentParser(description="Restore a backup to the local DAOS setup.")
parser.add_argument('--debug',
                    dest='debug', action='store_true',
                    help="enable debug output")
parser.add_argument('--contents',
                    dest='contents', default="/var/lib/daos/backup.list",
                    help="set the contents list file")
parser.add_argument('input',
                    help="the input file")

args = parser.parse_args()
if args.debug:
    root_logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(name)-16s %(message)s')
    handler.setFormatter(formatter)
    root_logger.setLevel(logging.DEBUG)
    root_logger.addHandler(handler)


backup = zipfile.ZipFile(args.input, 'r')
try:
    metadata: typing.Dict[str, typing.Dict[str, typing.Any]] = from_json(backup.read("metadata.json"))
except KeyError:
    metadata: typing.Dict[str, typing.Dict[str, typing.Any]] = dict()
directory_apply: typing.Dict[Path, typing.Dict[str, typing.Any]] = dict()
if args.contents:
    contents = open(args.contents, 'w')
else:
    contents = None


def add_to_contents(file: Path):
    if not contents:
        return
    escaped = str(file)
    escaped = escaped.encode("unicode_escape").decode("utf-8", "ignore")
    contents.write(escaped + "\n")


def archive_name(file: Path) -> str:
    target_name = str(file)
    return target_name[1:]


def remove_file(file: Path):
    try:
        if file.is_symlink():
            _LOGGER.debug(f"Removing symlink {file}")
            file.unlink()
        elif file.is_dir():
            _LOGGER.debug(f"Removing directory {file}")
            shutil.rmtree(str(file), ignore_errors=True)
        else:
            _LOGGER.debug(f"Removing file {file}")
            file.unlink()
    except FileNotFoundError:
        pass


def resolve_owner_group(meta: typing.Dict[str, typing.Any]) -> typing.Tuple[int, int]:
    owner = meta.get('owner')
    if owner:
        try:
            owner = pwd.getpwnam(owner).pw_uid
        except KeyError:
            owner = None
    if owner is None:
        owner = meta.get('uid')
    if owner is None:
        owner = -1
    else:
        owner = int(owner)

    group = meta.get('group')
    if group:
        try:
            group = grp.getgrnam(group).gr_gid
        except KeyError:
            group = None
    if group is None:
        group = meta.get('gid')
    if group is None:
        group = -1
    else:
        group = int(group)

    return owner, group


def apply_real_file(file: Path, meta: typing.Dict[str, typing.Any]):
    uid, gid = resolve_owner_group(meta)
    if uid != -1 or gid != -1:
        try:
            os.chown(file, uid, gid)
        except PermissionError:
            _LOGGER.warning(f"Failed to set ownership for {file}")
    if 'mode' in meta:
        try:
            os.chmod(file, int(meta['mode'], 8))
        except PermissionError:
            _LOGGER.warning(f"Failed to set mode for {file}")
    if 'mtime_ns' in meta:
        mtime = int(meta['mtime_ns'])
        try:
            os.utime(file, ns=(mtime, mtime))
        except OSError:
            _LOGGER.warning(f"Failed to set modification time for {file}")


def create_parents(file: Path):
    parents: typing.List[Path] = list()
    for p in file.parents:
        parents.append(p)
    if len(parents) < 2:
        return
    for p in reversed(parents[:-1]):
        parent_meta = metadata.get(str(p))
        try:
            os.mkdir(p, mode=0o755)
        except FileExistsError:
            continue
        except (PermissionError, OSError):
            _LOGGER.debug(f"Failed to create directory {p}", exc_info=True)
            continue
        if not parent_meta:
            _LOGGER.debug(f"Created parent directory {p}")
            continue
        _LOGGER.debug(f"Restored parent directory {p}")
        directory_apply[p] = parent_meta


def empty_target(file: Path):
    if file.is_symlink():
        file.unlink()
    elif file.is_dir():
        shutil.rmtree(str(file), ignore_errors=True)
    else:
        try:
            file.unlink()
        except FileNotFoundError:
            pass


def restore_symlink(file: Path, meta: typing.Dict[str, typing.Any]):
    target = backup.read(archive_name(file)).decode('utf-8')
    create_parents(file)
    empty_target(file)
    file.symlink_to(target)
    _LOGGER.debug(f"Symlink {file} to {target}")
    uid, gid = resolve_owner_group(meta)
    if uid != -1 or gid != -1:
        try:
            os.lchown(file, uid, gid)
        except PermissionError:
            _LOGGER.warning(f"Failed to set ownership for {file}")
    if 'mtime_ns' in meta:
        try:
            mtime = meta['mtime_ns']
            os.utime(file, ns=(mtime, mtime), follow_symlinks=False)
        except OSError:
            pass


def restore_directory(file: Path, meta: typing.Dict[str, typing.Any]):
    create_parents(file)
    if file.is_symlink():
        _LOGGER.debug(f"Removing symlink for directory replacement at {file}")
        file.unlink()
    elif file.exists():
        if not file.is_dir():
            _LOGGER.debug(f"Removing non-directory for replacement at {file}")
            file.unlink()
    _LOGGER.debug(f"Create directory {file}")
    try:
        os.mkdir(file)
    except FileExistsError:
        pass
    directory_apply[file] = meta


def restore_fifo(file: Path, meta: typing.Dict[str, typing.Any]):
    create_parents(file)
    empty_target(file)
    _LOGGER.debug(f"Create FIFO {file}")
    os.mkfifo(file)
    apply_real_file(file, meta)


def restore_cdev(file: Path, meta: typing.Dict[str, typing.Any]):
    major, minor = backup.read(archive_name(file)).split(b':', 1)
    major = int(major)
    minor = int(minor)

    create_parents(file)
    empty_target(file)
    _LOGGER.debug(f"Create character device {file} {major}:{minor}")
    os.mknod(file, mode=0o600 | stat.S_IFCHR, device=os.makedev(major, minor))
    apply_real_file(file, meta)


def restore_bdev(file: Path, meta: typing.Dict[str, typing.Any]):
    major, minor = backup.read(archive_name(file)).split(b':', 1)
    major = int(major)
    minor = int(minor)

    create_parents(file)
    empty_target(file)
    _LOGGER.debug(f"Create block device {file} {major}:{minor}")
    os.mknod(file, mode=0o600 | stat.S_IFBLK, device=os.makedev(major, minor))
    apply_real_file(file, meta)


def restore_socket(file: Path, meta: typing.Dict[str, typing.Any]):
    create_parents(file)
    empty_target(file)
    _LOGGER.debug(f"Create socket {file}")
    os.mknod(file, mode=0o777 | stat.S_IFSOCK)
    apply_real_file(file, meta)


def restore_regular(file: Path, meta: typing.Dict[str, typing.Any]):
    create_parents(file)
    empty_target(file)
    _LOGGER.debug(f"Restore regular file {file}")
    backup.extract(archive_name(file), "/")
    apply_real_file(file, meta)


for file in sorted(metadata.keys()):
    if not file.startswith("/"):
        continue
    if len(file) < 2 or file == "/." or file == "/..":
        continue

    file_meta = metadata[file]
    file = Path(file)

    file_type = file_meta['type'].lower()
    if file_type == "remove":
        remove_file(file)
    elif file_type == "weakdirectory":
        continue
    elif file_type == "symlink":
        restore_symlink(file, file_meta)
    elif file_type == "directory":
        restore_directory(file, file_meta)
    elif file_type == "fifo":
        restore_fifo(file, file_meta)
    elif file_type == "cdev":
        restore_cdev(file, file_meta)
    elif file_type == "bdev":
        restore_cdev(file, file_meta)
    elif file_type == "socket":
        restore_socket(file, file_meta)
    else:
        restore_regular(file, file_meta)
    add_to_contents(file)


for file in backup.infolist():
    if file.filename == "metadata.json":
        continue

    full_path = file.filename
    if full_path.endswith('/'):
        full_path = full_path[:-1]
    full_path = '/' + full_path
    if len(full_path) <= 1:
        continue
    file_meta = metadata.get(full_path)
    if file_meta:
        continue
    file_target = Path(full_path)

    try:
        st = file_target.stat()
        restore_uid = st.st_uid
        restore_gid = st.st_gid
        restore_mode = st.st_mode
    except FileNotFoundError:
        restore_uid = None
        restore_gid = None
        restore_mode = None

    _LOGGER.debug(f"Restoring {file.filename} with no metadata")
    create_parents(file_target)
    backup.extract(file, "/")

    if restore_uid is not None:
        try:
            os.chown(file_target, restore_uid, restore_gid)
        except PermissionError:
            _LOGGER.warning(f"Failed to reset ownership for {file_target}")
    if restore_mode is not None:
        try:
            os.chmod(file_target, restore_mode)
        except PermissionError:
            _LOGGER.warning(f"Failed to reset mode for {file_target}")

    add_to_contents(file_target)


for file in reversed(sorted(directory_apply.keys())):
    meta = directory_apply[file]
    _LOGGER.debug(f"Apply directory settings to {file}")
    apply_real_file(file, meta)


contents.close()
backup.close()

