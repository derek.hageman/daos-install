#!/usr/bin/python3

import typing
import logging
import argparse
import zipfile
import os
import pwd
import grp
from pathlib import Path
from json import dumps as to_json

_LOGGER = logging.getLogger(__name__)


parser = argparse.ArgumentParser(description="Create a backup of the local DAOS setup.")
parser.add_argument('--debug',
                    dest='debug', action='store_true',
                    help="enable debug output")
parser.add_argument('--contents',
                    dest='contents', default="/var/lib/daos/backup.list",
                    help="set the contents list file")
parser.add_argument('output',
                    help="the output file")

args = parser.parse_args()
if args.debug:
    root_logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(name)-16s %(message)s')
    handler.setFormatter(formatter)
    root_logger.setLevel(logging.DEBUG)
    root_logger.addHandler(handler)


metadata: typing.Dict[str, typing.Dict[str, typing.Any]] = dict()
backup = zipfile.ZipFile(args.output, 'w', compression=zipfile.ZIP_DEFLATED)


def archive_name(file: Path) -> str:
    target_name = str(file)
    return target_name[1:]


def add_erasure(file: Path):
    _LOGGER.debug(f"Adding erasure for {file}")
    metadata[str(file)] = {
        "type": "remove",
    }


def apply_real_file(file: Path):
    try:
        metadata[str(file)]['owner'] = file.owner()
    except KeyError:
        _LOGGER.debug(f"Owner for {file} not found")
    try:
        metadata[str(file)]['group'] = file.group()
    except KeyError:
        _LOGGER.debug(f"Group for {file} not found")
    st = file.stat()
    metadata[str(file)]['uid'] = st.st_uid
    metadata[str(file)]['gid'] = st.st_gid
    metadata[str(file)]['mtime_ns'] = st.st_mtime_ns
    metadata[str(file)]['mode'] = '%o' % st.st_mode


def add_parents(file: Path):
    parents: typing.List[Path] = list()
    for p in file.parents:
        parents.append(p)
    if len(parents) < 2:
        return
    for p in parents[:-1]:
        if str(p) in metadata:
            break
        if not p.is_dir():
            break
        metadata[str(p)] = {
            "type": "weakdirectory",
        }
        apply_real_file(p)


def add_symlink(file: Path):
    try:
        target = os.readlink(file)
        st = file.lstat()
    except OSError:
        _LOGGER.warning("Failed to read link", exc_info=True)
        return
    _LOGGER.debug(f"Adding symlink for {file} to {target}")
    add_parents(file)
    metadata[str(file)] = {
        "type": "symlink",
        "uid": st.st_uid,
        "gid": st.st_gid,
        "mtime_ns": st.st_mtime_ns,
    }
    try:
        metadata[str(file)]['owner'] = pwd.getpwuid(st.st_uid).pw_name
    except KeyError:
        _LOGGER.debug(f"Owner for {file} not found")
    try:
        metadata[str(file)]['group'] = grp.getgrgid(st.st_gid).gr_name
    except KeyError:
        _LOGGER.debug(f"Group for {file} not found")
    backup.writestr(archive_name(file), target.encode('utf-8'))


def add_dir(file: Path):
    _LOGGER.debug(f"Adding directory at {file}")
    add_parents(file)
    metadata[str(file)] = {
        "type": "directory",
    }
    apply_real_file(file)
    try:
        backup.mkdir(archive_name(file))
    except AttributeError:
        pass


def add_fifo(file: Path):
    _LOGGER.debug(f"Adding fifo for {file}")
    add_parents(file)
    metadata[str(file)] = {
        "type": "fifo",
    }
    apply_real_file(file)
    backup.writestr(archive_name(file), "FIFO")


def add_cdev(file: Path):
    try:
        st = os.stat(file)
        major = os.major(st.st_rdev)
        minor = os.minor(st.st_cdev)
    except OSError:
        _LOGGER.warning("Failed to read device", exc_info=True)
        return
    _LOGGER.debug(f"Adding character device for {file}")
    add_parents(file)
    metadata[str(file)] = {
        "type": "cdev",
    }
    apply_real_file(file)
    backup.writestr(archive_name(file), f"{major}:{minor}")


def add_bdev(file: Path):
    try:
        st = os.stat(file)
        major = os.major(st.st_rdev)
        minor = os.minor(st.st_cdev)
    except OSError:
        _LOGGER.warning("Failed to read device", exc_info=True)
        return
    _LOGGER.debug(f"Adding block device for {file}")
    add_parents(file)
    metadata[str(file)] = {
        "type": "bdev",
    }
    apply_real_file(file)
    backup.writestr(archive_name(file), f"{major}:{minor}")


def add_socket(file: Path):
    _LOGGER.debug(f"Adding socket at {file}")
    add_parents(file)
    metadata[str(file)] = {
        "type": "socket",
    }
    apply_real_file(file)
    backup.writestr(archive_name(file), "SOCKET")


def add_regular(file: Path):
    _LOGGER.debug(f"Adding file {file}")
    add_parents(file)
    metadata[str(file)] = {
        "type": "file",
    }
    apply_real_file(file)
    target_name = str(file)
    target_name = target_name[1:]
    backup.write(str(file), target_name)


with open(args.contents, 'r') as file_list:
    for filename in file_list:
        filename = filename.lstrip()
        while filename.endswith("\n") or filename.endswith("\r"):
            filename = filename[:-1]
        if not filename:
            continue
        if not filename.startswith('/'):
            _LOGGER.warning(f"File {filename} must start with a /")
            continue
        unescaped = filename.encode('ascii', 'ignore').decode('unicode_escape')
        file = Path(unescaped)
        if file.is_symlink():
            add_symlink(file)
        elif not file.exists():
            add_erasure(file)
        elif file.is_dir():
            add_dir(file)
        elif file.is_fifo():
            add_fifo(file)
        elif file.is_char_device():
            add_cdev(file)
        elif file.is_block_device():
            add_bdev(file)
        elif file.is_socket():
            add_socket(file)
        elif file.is_file():
            add_regular(file)
        else:
            _LOGGER.warning(f"Ignoring unsupported file type on {file}")


_LOGGER.debug(f"Backup contains {len(metadata)} metadata entries")
backup.writestr("metadata.json", to_json(
    metadata,
    indent=4,
    sort_keys=True,
))
backup.close()

if args.debug:
    backup = Path(args.output)
    _LOGGER.debug(f"Total backup size {backup.stat().st_size} bytes")
