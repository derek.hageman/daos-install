#!/usr/bin/python3

import typing
import logging
import argparse
import os
import sys
import tempfile
import stat
from pathlib import Path

_LOGGER = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description="Create a backup of the local DAOS setup.")
parser.add_argument('--debug',
                    dest='debug', action='store_true',
                    help="enable debug output")
parser.add_argument('--no-sudo',
                    dest='no_sudo', action='store_true',
                    help="disable automatic sudo")
parser.add_argument('--contents',
                    dest='contents', default="/var/lib/daos/backup.list",
                    help="set the contents list file")
parser.add_argument('-R', '--recursive',
                    dest='recursive', action='store_true',
                    help="recursively add contents")
parser.add_argument('add',
                    help="the file or directory to add",
                    nargs='+')


args = parser.parse_args()
if args.debug:
    root_logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(name)-16s %(message)s')
    handler.setFormatter(formatter)
    root_logger.setLevel(logging.DEBUG)
    root_logger.addHandler(handler)


if not args.no_sudo and os.getuid() != 0:
    _LOGGER.debug("User ID is non-zero, sudo self")
    os.execvp("sudo", ["sudo", "--"] + sys.argv)


contents_file = Path(args.contents)

backup_contents: typing.Set[str] = set()
try:
    with contents_file.open('r') as file_list:
        for filename in file_list:
            filename = filename.lstrip()
            while filename.endswith("\n") or filename.endswith("\r"):
                filename = filename[:-1]
            if not filename:
                continue
            if not filename.startswith('/'):
                _LOGGER.warning(f"File {filename} must start with a /")
                continue
            backup_contents.add(filename)
except FileNotFoundError:
    pass

for add_file in args.add:
    add_file = Path(add_file)
    _LOGGER.debug(f"Adding {add_file}")
    if args.recursive and add_file.is_dir():
        backup_contents.add(str(add_file.absolute()))
        for sub_file in add_file.rglob("*"):
            _LOGGER.debug(f"Recursed to {sub_file}")
            backup_contents.add(str(sub_file.absolute()))
    else:
        backup_contents.add(str(add_file.absolute()))

output_fd, output_list = tempfile.mkstemp(dir=contents_file.parent)
for add_file in sorted(backup_contents):
    os.write(output_fd, add_file.encode('utf-8') + b"\n")
os.close(output_fd)

try:
    os.unlink(contents_file)
except FileNotFoundError:
    pass
os.rename(output_list, contents_file)
contents_file.chmod(stat.S_IRUSR | stat.S_IWUSR)
