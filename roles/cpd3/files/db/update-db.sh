#!/bin/sh

DATAFILE=`mktemp`

rm -f cpd3.db >/dev/null 2>&1

da.get _ output_cpd2,displays,ebas,netcdf,site,import_ebas,contamination,editing,update_plots,processing,transfer,upload,realtime forever configuration > $DATAFILE
CPD3ARCHIVE="sqlite:cpd3.db" da.archive --remove=none $DATAFILE

#da.get _ bootstrap forever bootstrap > $DATAFILE
#CPD3ARCHIVE="sqlite:cpd3.db" da.archive --remove=none $DATAFILE

da.get allstations alias forever allarchives > $DATAFILE
CPD3ARCHIVE="sqlite:cpd3.db" da.archive --remove=none $DATAFILE

FILES=$(find config -mindepth 1 -maxdepth 1 -type f)
for fil in $FILES; do
    CPD3ARCHIVE="sqlite:cpd3.db" da.archive --remove=none "${fil}"
done

rm -f $DATAFILE
