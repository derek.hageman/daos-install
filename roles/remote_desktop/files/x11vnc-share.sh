#!/bin/sh

systemctl --user import-environment DISPLAY XAUTHORITY

exec systemctl --user start x11vnc.service