#!/usr/bin/env python3

import typing
import tomlkit
from importlib import import_module
from pathlib import Path
from subprocess import run
from dynaconf import Dynaconf
from dynaconf.constants import DEFAULT_SETTINGS_FILES

CONFIG_PATH = "/etc/forge/acquisition/settings.local.toml"

run(["featherpad", CONFIG_PATH])


def check_config() -> typing.Optional[str]:
    try:
        with open(CONFIG_PATH, "rt") as f:
            try:
                tomlkit.load(f)
            except tomlkit.exceptions.ParseError as e:
                return f"Configuration syntax error: <tt>{str(e)}</tt>"
    except FileNotFoundError:
        return "File not found"

    try:
        config = Dynaconf(
            root_path=str(Path(CONFIG_PATH).parent),
            environments=False,
            lowercase_read=False,
            merge_enabled=True,
            default_settings_paths=DEFAULT_SETTINGS_FILES,
        )
        instrument_root = config.get("INSTRUMENT")
    except Exception as e:
        return f"Configuration syntax error: <tt>{str(e)}</tt>"

    if not instrument_root or len(list(instrument_root.keys())) == 0:
        return "No instruments defined"
    for source in instrument_root.keys():
        instrument_type = config.get(f"INSTRUMENT.{source}.TYPE")
        if not instrument_type:
            return f"Instrument <b>{source}</b> has no type set"
        if '/' in instrument_type:
            continue
        elif '..' in instrument_type:
            return f"Instrument <b>{source}</b> has invalid type <tt>{instrument_type}</tt>"

        try:
            import_module('.', 'forge.acquisition.instrument.' + instrument_type).main
        except (ImportError, AttributeError, ModuleNotFoundError):
            return f"Instrument <b>{source}</b> has invalid type <tt>{instrument_type}</tt>"

    return None


if error := check_config():
    run(["zenity", "--warning", "--title", "Configuration Error", "--text", error])
    exit(1)
