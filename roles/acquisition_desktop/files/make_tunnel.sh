#!/bin/bash

journalctl -u forge-telemetry-tunnel.service -f -n 0 &
LOG_PID=$!

if ! sudo systemctl start forge-telemetry-tunnel.service; then
  kill $LOG_PID
  clear
  journalctl -u forge-telemetry-tunnel.service -n 50
  echo -e "\n\nError starting tunnel service"
  read TEMP
  exit 1
fi

echo -e "\n\nPress ENTER to close the tunnel and exit\n\n"
read TEMP

kill $LOG_PID
sudo systemctl stop forge-telemetry-tunnel.service
