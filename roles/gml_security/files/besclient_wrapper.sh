#!/bin/sh

# XXX Extracted from the RPM data

# /etc/init.d/besclient generated at installation time to wrap systemctl commands

export SYSTEMD_NO_WRAP=1

case "$1" in
        start|stop|try-restart|restart|reload|status)
                /bin/systemctl "$1" besclient
                ;;
        force-reload)
                /bin/systemctl reload-or-restart besclient
                ;;
        *)
                /bin/echo "Usage: $0 {start|stop|status|try-restart|restart|force-reload|reload}"
                exit 1
                ;;
esac
chmod +rx /etc/init.d/besclient