#!/bin/bash

LOCAL_USER=$(whoami)

TERMINAL_COMMAND='

LOCAL_USER="$1"
DISPLAY="$2"
WAYLAND_DISPLAY="$3"

echo "\n\nNAG NAG NAG\n\n"
echo "Get Chris or Gregg to come over and set the crest user password."
echo "Or, do not and close this window and they will be very disappointed with you and you will be bothered again later anyway."
echo "\n\n\n"

/usr/local/bin/set_crest_password.sh || exit 1
sudo -u $LOCAL_USER systemctl --user disable crest-password.timer
sudo -u $LOCAL_USER systemctl --user stop crest-password.timer
'

if [ -z "$DISPLAY" ]; then
  export DISPLAY=":0"
fi

if [ -t 0 ]; then
  exec sudo /bin/sh -c "$TERMINAL_COMMAND" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY"
elif command -v xfce4-terminal >/dev/null; then
  exec xfce4-terminal -x sudo /bin/sh -c "$TERMINAL_COMMAND" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY"
elif command -v lxterminal >/dev/null; then
  exec lxterminal -e sudo /bin/sh -c "$TERMINAL_COMMAND" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY"
elif command -v gnome-terminal >/dev/null; then
  exec gnome-terminal -- sudo /bin/sh -c "$TERMINAL_COMMAND" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY"
elif command -v konsole >/dev/null; then
  exec konsole -e sudo /bin/sh -c "$TERMINAL_COMMAND" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY"
elif command -v xterm >/dev/null; then
  exec xterm -e sudo /bin/sh -c "$TERMINAL_COMMAND" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY"
else
  exec sudo /bin/sh -c "$TERMINAL_COMMAND" sh "$LOCAL_USER" "$DISPLAY" "$WAYLAND_DISPLAY"
fi
