# Restricted more in the masquerade tables
add element ip6 filter accept-tcp { domain }
add element ip filter accept-tcp { domain }
add element ip6 filter accept-udp { domain, dhcpv6-server }
add element ip filter accept-udp { domain, bootps }

table ip wireless {
	set reject-external-tcp {
		type inet_service;
	}
	set reject-external-udp {
		type inet_service;
	}

	chain postrouting {
		type nat hook postrouting priority -100;

		# Normal routing
		oif { lo, {{ wlan_interface }} } return

		masquerade
	}

	chain rejected-external {
		counter comment "rejected external"
		limit rate 30/minute reject with icmp type admin-prohibited
		drop
	}
	chain input {
		type filter hook input priority 10;

		# Accept all internal
		iif { lo, {{ wlan_interface }} } return

		# Accept existing
		ct state established,related accept
		# Drop invalid
		ct state invalid drop
		# All connections are new now

		tcp dport @reject-external-tcp goto rejected-external
		udp dport @reject-external-udp goto rejected-external
	}
}

table ip6 wireless {
	set reject-external-tcp {
		type inet_service;
	}
	set reject-external-udp {
		type inet_service;
	}

	chain postrouting {
		type nat hook postrouting priority -100;

		# Normal routing
		oif { lo, {{ wlan_interface }} } return

		masquerade
	}

	chain rejected-external {
		counter comment "rejected external"
		limit rate 30/minute reject with icmpv6 type admin-prohibited
		drop
	}
	chain input {
		type filter hook input priority 10;

		# Accept all internal
		iif { lo, {{ wlan_interface }} } return

		# Accept existing
		ct state established,related accept
		# Drop invalid
		ct state invalid drop
		# All connections are new now

		tcp dport @reject-external-tcp goto rejected-external
		udp dport @reject-external-udp goto rejected-external
	}
}

# Now reject these from the external side
add element ip6 wireless reject-external-tcp { domain }
add element ip wireless reject-external-tcp { domain }
add element ip6 wireless reject-external-udp { domain, dhcpv6-server }
add element ip wireless reject-external-udp { domain, bootps }
