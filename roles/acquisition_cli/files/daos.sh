#!/bin/sh

set -a
. "/etc/default/cpd3-acquisition"
set +a

cpd() {
    case "$1" in
    -c )
        ROOT_PATH_FOR_DYNACONF=/etc/forge/acquisition /opt/forge/venv/bin/forge-acquisition-console
        ;;
    -v )
        DISPLAY= vi /etc/forge/acquisition/settings.local.toml
        ;;
    -r | -r )
        sudo systemctl restart forge-acquisition.service
        ;;
    -n )
        chronyc tracking
        ;;
    -s )
        sudo systemctl start forge-transfer-data.service
        ;;
    -e )
        shift
        /opt/forge/venv/bin/forge-acquisition-serial-eavesdropper "$@"
        ;;
    * )
        echo "Usage: cpd [operation]
    -c      Show the 'blue screen' console client
    -v      Edit configuration with vi
    -e      Run the eavesdropper
    -s      Send data
    -n      Display NTP information
    -r      Restart acquisition system"
        ;;
    esac
}

alias fdc="/opt/forge/venv/bin/forge-data-command"
