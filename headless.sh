#!/bin/sh

LOCAL_USER=$(whoami)

if [ "$LOCAL_USER" = "root" ]; then
    echo "Please run as the user you intend to log in as"
    exit 1
fi

echo "\n\n\nDAOS bootstrap starting\n\n"
sleep 1

BOOTSTRAP_ROOT='
fail() {
  echo "\n\n**** ERROR - BOOTSTRAP INCOMPLETE *****\n\nPress ENTER to continue\n"
  exit 1
}

LOCAL_USER="$1"

apt-get update
apt-get upgrade -y
apt-get install -y ansible git || fail

rm -rf /usr/share/daos >/dev/null 2>&1 </dev/null || true
git clone https://gitlab.com/derek.hageman/daos-install.git /usr/share/daos || fail

cd /usr/share/daos || fail
ansible-playbook -i local_inventory -e "username=$LOCAL_USER" headless.yml || fail

echo "\n\n\nBootstrap finished.\n"
echo "Please reboot NOW before proceeding with any further setup"
'

exec sudo /bin/sh -c "$BOOTSTRAP_ROOT" sh "$LOCAL_USER"
