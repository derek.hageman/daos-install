- hosts: localhost
  vars_prompt:
    - name: station
      prompt: |-
        The setup process requires a GAW station code.  This is a three character 
        identifier assigned to a physical location that identifies where the data 
        was acquired.  It is also used to download any existing configuration for 
        the station.
        
        GAW station code
      private: no
    - name: acquisition_uplink
      prompt: |-
        The acquisition system can be connected to the NOAA servers in real time.
        This setting control the amount of realtime data that is sent to the
        server.  When connected to the NOAA server, this allows authorized users
        to remotely operate the acquisition system and plot realtime data from the
        web interface, without needing to log in to the acquisition computer itself.
        For most stations, the recommended level (1) consumes less than 100 KiB per
        hour.  However, if the station has unreliable or limited internet, use
        level 0 to disable the realtime data.
        
        0 - Disable realtime data link
        1 - Use averaged realtime data (RECOMMENDED)
        2 - Full instantaneous realtime data
        
        Realtime data level [1]
      private: no
    - name: telemetry_level
      prompt: |-
        Once set up, the system will automatically report basic system telemetry
        (e.x. IP address) back to the NOAA servers.  This setting configures the
        detail of telemetry reported to NOAA.  Higher settings consume more
        internet bandwidth.  For stations with a reliable internet connection
        use the default (1); for stations without reliable internet or with highly
        restricted connections, use level 0.
        
        0 - Telemetry reported once per day
        1 - Basic telemetry reported semi-frequently (RECOMMENDED)
        2 - Full telemetry reported continuously
        
        Telemetry level [1]
      private: no
    - name: automatic_tunnel
      prompt: |-
        DAOS includes a remote access tunnel to NOAA servers even when the 
        acquisition system is not accessible from the public internet.  This is 
        used to allow for maintenance or troubleshooting from NOAA.  The tunnel can
        be operated in two modes: always on or by requiring a local desktop user to
        click an icon.
        
        For stations with reliable internet connections, using the always on mode
        is highly recommended.
        
        Enable always on tunnel [Y/n]
      private: no
  tasks:
    - name: Set variables
      set_fact:
        station: "{% if (station | regex_search('^[A-Za-z][A-Za-z0-9_]{2,}$')) %}{{ station|upper }}{% else %}NIL{% endif %}"
        acquisition_uplink: "{% if acquisition_uplink | regex_search('^[0-9]+$') %}{{ acquisition_uplink|int }}{% else %}1{% endif %}"
        telemetry_level: "{% if telemetry_level | regex_search('^[0-9]+$') %}{{ telemetry_level|int }}{% else %}1{% endif %}"
        automatic_tunnel: "{% if automatic_tunnel | regex_search('^[0-9]+$') %}{% if automatic_tunnel|int == 0 %}false{% else %}true{% endif %}{% elif automatic_tunnel | regex_search('^[nNfF]') %}false{% else %}true{% endif %}"

- hosts: localhost
  tasks:
    - name: Check default desktop environment
      when: xdg_gui is defined and xdg_gui != "" and xdg_gui != "wlroots"
      set_fact:
        daos_gui: "{{ xdg_gui|lower }}"
    - name: Detect XFCE install
      when:
        - not (xdg_gui is defined and xdg_gui != "" and xdg_gui != "wlroots")
        - ansible_distribution == 'Ubuntu'
      set_fact:
        daos_gui: "xfce"
    - name: Check RaspberryPi issue
      stat:
        path: /etc/rpi-issue
        get_checksum: False
        get_attributes: False
        get_mime: False
      register: rpi_issue
    - name: Detect LXDE install
      when:
        - not (xdg_gui is defined and xdg_gui != "" and xdg_gui != "wlroots")
        - ansible_distribution == 'Debian'
        - rpi_issue.stat.exists
      set_fact:
        daos_gui: "lxde"
    - name: Get user information
      user:
        name: "{{ username }}"
      register: user_info
    - name: Set user information variable
      set_fact:
        userid: "{{ user_info.uid }}"

- name: Setup Forge acquisition
  import_playbook: forge_acquisition.yml
  vars:
    username: "{{ username }}"
    station: "{{ station }}"
    acquisition_uplink: "{{ acquisition_uplink }}"
    telemetry_level: "{{ telemetry_level }}"
    automatic_tunnel: "{{ automatic_tunnel }}"

- hosts: localhost
  vars:
    desktop_env:
      DISPLAY: "{{ lookup('env', 'DISPLAY')|default(':0.0', True) }}"
      WAYLAND_DISPLAY: "{{ lookup('env', 'WAYLAND_DISPLAY')|default('wayland-0', True) }}"
      XAUTHORITY: "{{ lookup('env', 'XAUTHORITY')|default('/home/' + username +'/.Xauthority', True) }}"
      DBUS_SESSION_BUS_ADDRESS: "{{ lookup('env', 'DBUS_SESSION_BUS_ADDRESS')|default('unix:path=/run/user/' + (userid|string) + '/bus', True) }}"
  roles:
    - role: acquisition_cli
    - role: acquisition_desktop

- name: Restore backup
  when:
    - (lookup('env', 'DAOS_BACKUP') | regex_search('^(n|no|off|0|false|f)$', ignore_case=True)) == None
    - (lookup('env', 'DAOS_BACKUP') | regex_replace('^$', station) | lower) != 'nil'
  import_playbook: restore_backup.yml
  vars:
    station: "{{ station }}"

- name: Complete setup
  import_playbook: complete_setup.yml
  vars:
    username: "{{ username }}"